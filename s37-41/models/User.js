const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required : [true, "First Name is required"]
	},
	lastName : {
		type: String,
		required : [true, "Last Name is required"]
	},
	email : {
		type: String,
		required : [true, "Email is required"]
	},
	password : {
		type: String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type: Boolean,
		default : false,
		required : [true, "isAdmin is required"]
	},
	mobileNo : {
		type: String,
		required : [true, "Mobile Number is required"]
	},
	enrollments : [
		{
			courseId : {
				type : String,
				required : [true, "courseId is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date(),
				required : [true, "enrolledOn is required"]
			},
			status : {
				type : String,
				default : "Enrolled",
				required : [true, "Status is required"]
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);