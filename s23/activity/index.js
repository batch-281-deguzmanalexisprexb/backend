// console.log("Hello");

// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!");
	}
};
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square brackcet notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
trainer.talk();

function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target) {
		let _targetPokemonHealth_ = target.health - this.attack;
		console.log(`${this.name} tackled ${target.name}`);
		console.log(`${target.name}'s health is now reduced to ${_targetPokemonHealth_}`);
		target.health = _targetPokemonHealth_;

		if(_targetPokemonHealth_ <= 0){
			this.faint(target);
		}

		console.log(target);
	},

	this.faint = function(target) {
		console.log(`${target.name} fainted`);
	}
	
	console.log(this);
}

let pikachu = new Pokemon("Pikachu", 12);
// console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
// console.log(geodude);
let jigglypuff = new Pokemon("Jigglypuff", 100);
// console.log(jigglypuff);

geodude.tackle(pikachu);

// console.log(pikachu);

jigglypuff.tackle(geodude);

// console.log(geodude);







//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}