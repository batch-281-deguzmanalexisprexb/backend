const User = require("../models/UserModel");

module.exports.checkEmailExists = (req, res) => {
	const {email} = req.body;

	User.find({email})
		.then(result => {
			if(result.length > 0){
				res.send(true);
			} else {
				res.send(false);
			}
		})
		.catch(error => {
			console.error("Error Checking email", error);
			res.status(500).send("An error occured while checking the email")
		})
};

module.exports.registerUser = (req, res) => {
	let newUser = new User ({
		...req.body,
	})

	return newUser.save()
		.then(user => {
			console.log(user);
			res.send(true);
		})
		.catch(error => {
			res.send(false);
		})
};

/*module.exports.loginUser = (req, res) => {
	
}*/