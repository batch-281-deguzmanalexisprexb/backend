const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv").config();
const cloudinary = require("cloudinary");

cloudinary.config({
	cloud_name: process.env.CLOUD_NAME,
	api_key: process.env.CLOUD_API_KEY,
	api_secret: process.env.CLOUD_API_SECRET
})

// Import routes here
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const purchasedOrderRoutes = require("./routes/purchasedOrderRoutes");


const app = express();

// Database connection
mongoose.connect(process.env.DB_URL,
		{
			useNewUrlParser : true,
			useUnifiedTopology : true
		}
	);
let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection Error."));
db.once('open', () => console.log("Now connected to mongodb Atlas"));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended : true}));
app.delete('/:public_id', async(req,res) =>{
	const {public_id} = req.params;
	try{
		await cloudinary.uploader.destroy(public_id);
		res.status(200).send();
	} catch (error) {
		res.status(400).send();
	}
})

// Route Registration
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/purchasedOrders", purchasedOrderRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
})