const mongoose = require("mongoose");

const purchasedOrderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required : [true, "Product ID is required"]
			},
			quantity: {
				type: Number,
				required : [true, "Quantity is required"]
			}
		}
	],
	totalAmount: {
		type: Number,
		required : [true, "Total Amaount is required"]
	},
	purchasedOn: {
		type: Date,
		default: Date.now,
		required : [true, "Purchase Date is required"]
	},
	isPaid : {
		type: Boolean,
		default : false,
		required : [true, "isPaid is required"]
	},
	isDelivered : {
		type: Boolean,
		default : false,
		required : [true, "isDelivered is required"]
	}
})

module.exports = mongoose.model("PurchasedOrder", purchasedOrderSchema);