const mongoose = require("mongoose");

const userSchema = new mongoose.Schema ({
	email : {
		type: String,
		required : [true, "Email is required"]
	},
	password : {
		type: String,
		required : [true, "Email is required"]
	},
	isAdmin : {
		type: Boolean,
		default : false,
		required : [true, "isAdmin is required"]
	}
})

module.exports = mongoose.model("User", userSchema);