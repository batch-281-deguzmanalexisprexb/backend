const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	imagesUrl: [
		{
			url: {
				type: String
			},
			public_id: {
				type: String
			}
		}
	],
	name : {
		type: String,
		required : [true, "Name is required"]
	},
	description : {
		type: String,
		required : [true, "Description is required"]
	},
	price : {
		type: Number,
		required : [true, "Price is required"]
	},
	isActive : {
		type: Boolean,
		default: true,
		required : [true, "isActive is required"]
	},
	createdOn : {
		type: Date,
		default: Date.now,
		required : [true, "Date is required"]
	}
})

module.exports = mongoose.model("Product", productSchema);