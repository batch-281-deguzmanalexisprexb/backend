const Product = require("../models/ProductModel")
const auth = require("../auth")

// Create product (admin only)
module.exports.addProduct = async (req, res) => {
	const { imagesUrl, name, description, price } = req.body;
	const { isAdmin } = auth.decode(req.headers.authorization);

	try {
    	if (!isAdmin) {
			return res.status(403).send('You are not an admin');
    	}

    	const existingProduct = await Product.findOne({
			name: { $regex: new RegExp(`^${name}$`, 'i') }
    	});

    	if (existingProduct) {
			return res.status(409).send('Product with the same name already exists');
    	}

    	const newProduct = new Product({
    		imagesUrl,
			name,
			description,
			price
    	});

    	await newProduct.save();
    	return res.send(newProduct);
	} catch (error) {
    	console.error("Error adding product:", error);
    	return res.status(500).send("Error adding product");
	}
};

// Retrieve all products
module.exports.getAllProducts = (req, res) => {
	const { isAdmin } = auth.decode(req.headers.authorization);

	if ( isAdmin ) {
		return Product.find({})
			.then(result => res.send(result))
			.catch(error => res.send(error))
	} else {
		res.status(403).send('You are not an admin');
	}

	
};

// Retrieve all active products
module.exports.getAllActive = (req, res) => {
	return Product.find({isActive: true})
		.then(result => res.send(result))
		.catch(error => res.send(error))
};

// Retrieve single product
module.exports.getSingleProduct = (req, res) => {
	return Product.findById(req.params.productId)
		.then(result => res.send(result))
		.catch(error => res.send(error))
};

// Update product information (admin only)
module.exports.updateProduct = (req, res) => {
	const {isAdmin} = auth.decode(req.headers.authorization);

	if (isAdmin) {
		let updatedProduct = {
			...req.body
		};

		return Product.findByIdAndUpdate(req.params.productId, updatedProduct, { new: true })
			.then((result) => res.send(result))
			.catch(error => {
				console.error('Error updating product:', error);
				res.status(500).send('Error updating product');
			})
	} else {
		res.status(403).send('You are not an admin');
	}
};

// Archive product (admin only)
module.exports.archiveProduct = async (req, res) => {
	const { isAdmin } = auth.decode(req.headers.authorization);
	const productId = req.params.productId

	try {
		if ( !isAdmin ) {
			return res.status(403).send("You are not authorized to perform this action");
		}

		const product = await Product.findById(productId)
		if ( !product ) {
			return res.status(404).send("Product not found");
		}

		if ( !product.isActive ) {
			return res.status(400).send("Product is already archived");
		}

		product.isActive = false;
		await product.save();
		return res.send(product);
	} catch (error) {
		console.error('Error updating product:', error);
		res.status(500).send('Error updating product');
	}
};

// Activate Product (Admin only)
module.exports.activateProduct = async (req, res) => {
	const { isAdmin } = auth.decode(req.headers.authorization);
	const productId = req.params.productId

	try {
		if ( !isAdmin ) {
			return res.status(403).send("You are not authorized to perform this action");
		}

		const product = await Product.findById(productId)
		if ( !product ) {
			return res.status(404).send("Product not found");
		}

		if ( product.isActive ) {
			return res.status(400).send("Product is already active");
		}

		product.isActive = true;
		await product.save();
		return res.send(product);
	} catch (error) {
		console.error('Error updating product:', error);
		res.status(500).send('Error updating product');
	}
};

// Delete Product
module.exports.deleteProduct = async(req, res) => {
	const { isAdmin } = auth.decode(req.headers.authorization);
	const productId = req.params.productId

	try{
		if ( !isAdmin ) {
			return res.status(403).send("You are not authorized to perform this action");
		}

		const product = await Product.findById(productId)
		console.log(product);
		if ( !product ) {
			return res.status(404).send("Product not found");
		}

		await Product.deleteOne({ _id: productId });
		res.send(true)

	} catch (error) {
		console.error('Error deleting product:', error);
		res.status(500).send('Error deleting product');
	}
}