const Order = require("../models/OrderModel");
const User = require("../models/UserModel");
const Product = require("../models/ProductModel");

const auth = require("../auth");

// Create Order (Non-Admin)
module.exports.createOrder = async (req, res) => {
	const { productId, quantity } = req.body;
	const { isAdmin, id: userId } = auth.decode(req.headers.authorization);

	if ( !productId || !quantity ) {
		return res.status(400).send('All fields must be filled');
	}

	try {
		const product = await Product.findById(productId);
		if (!product) {
			return res.status(404).send("Product not found");
		}

		if (!product.isActive) {
			return res.status(403).send("Product is not active");
		}

		if (!isAdmin) {
			const newOrder = new Order({
				userId,
				products: [ { productId, quantity } ],
				totalAmount: quantity * product.price
			});

			await newOrder.save();
			return res.send(newOrder);
		} else {
			return res.status(403).send("You are an admin");
		}
	} catch (error) {
		console.error("Error adding order:", error);
		return res.status(500).send("Error adding order");
	}
};

// Retrieve all orders (admin only)
module.exports.getAllOrders = async (req, res) => {
	const { isAdmin } = auth.decode(req.headers.authorization);

	if (!isAdmin) {
		return res.status(403).send("You are not admin");
	}

	try{
		const orders = await Order.find()

		if (!orders) {
			return res.status(404).send("There is no existing Order");
		}

		return res.send(orders);

	} catch (error) {
		console.error("Error getting all orders:", error);
		return res.status(500).send("Error getting all orders");
	}
};

// Add to cart
// Added products
module.exports.addToCart = async (req, res) => {
	const { productId, quantity } = req.body;
	const { isAdmin, id: userId } = auth.decode(req.headers.authorization);

	if ( !productId || !quantity ) {
		return res.status(400).send('All fields must be filled');
	}

	try {
		const product = await Product.findById(productId);
		if ( isAdmin ) {
			return res.status(403).send("You are an admin");
		}

		if (!product) {
			return res.status(404).send("Product not found");
		}

		if (!product.isActive) {
			return res.status(403).send("Product is not active");
		}

		const existingOrder = await Order.findOne({userId});
		
		if (!existingOrder) {
			const newOrder = new Order({
				userId,
				products: [ { productId, quantity } ],
				totalAmount: quantity * product.price
			});

			await newOrder.save();
			return res.send(newOrder);
		}

		const existingOrderProduct = existingOrder.products.find(prod => prod.productId === productId );

		if (existingOrderProduct) {
			existingOrderProduct.quantity += quantity;
			existingOrder.totalAmount += quantity * product.price;

			await existingOrder.save();
			return res.send(existingOrder);
		}

		existingOrder.products.push({ productId, quantity });
        existingOrder.totalAmount += quantity * product.price;

        await existingOrder.save()
        return res.send(existingOrder);
	} catch (error) {
		console.error("Error adding to cart:", error);
		res.status(500).send("Error adding to cart");
	}
};

// Change product quantities
module.exports.changeProductQuantity = async (req, res) => {
	const { productId, quantity } = req.body;
	const { id: userId } = auth.decode(req.headers.authorization);

	if ( !productId || !quantity ) {
		return res.status(400).send('All fields must be filled');
	}

	try {
		const existingOrder = await Order.findOne({userId});
		if (!existingOrder || existingOrder.products.length === 0) {
			return res.status(404).send("There is no existing order")
		}

		const existingOrderProduct = existingOrder.products.find(prod => prod.productId === productId );
		if (!existingOrderProduct) {
			return res.status(404).send("No product match in cart");
		}

		const product = await Product.findById(productId);
		if (!product) {
			return res.status(404).send("Product not found");
		}

		const previousQuantity = existingOrderProduct.quantity;
		const previousTotalAmount = existingOrder.totalAmount;

		existingOrderProduct.quantity = quantity;
		existingOrder.totalAmount = previousTotalAmount - (previousQuantity * product.price) + (quantity * product.price);

		await existingOrder.save();
		return res.send(existingOrder);
	} catch (error) {
		console.error("Error editing the quantity of a product on cart:", error);
		res.status(500).send("Error editing the quantity of a product on cart");
	}
};

// Remove orders from cart
module.exports.removeProduct = async (req, res) => {
	const { productId } = req.params;
	const { id: userId } = auth.decode(req.headers.authorization);

	if ( !productId ) {
		return res.status(400).send('All fields must be filled');
	}

	try {
		const existingOrder = await Order.findOne({userId});
		if (!existingOrder  || existingOrder.products.length === 0) {
			return res.status(404).send("There is no existing order")
		}

		const existingOrderProduct = existingOrder.products.find(prod => prod.productId === productId );
		if (!existingOrderProduct) {
			return res.status(404).send("No product match in cart");
		}

		const product = await Product.findById(productId);
		if (!product) {
			return res.status(404).send("Product not found");
		}
		
		existingOrder.totalAmount -= existingOrderProduct.quantity * product.price
		existingOrder.products.pull(existingOrderProduct._id);
		await existingOrder.save();

		return res.send(existingOrder);
	} catch (error) {
		console.error("Error removing product on cart:", error);
		return res.status(500).send("Error removing product on cart");
	}
};

// Subtotal for each item
module.exports.getSubtotal = async (req, res) => {
	const { id: userId } = auth.decode(req.headers.authorization);

	try {
		const existingOrder = await Order.findOne({userId});
		if (!existingOrder) {
			return res.status(404).send("There is no existing order")
		}

		const ordersInCart = existingOrder.products;
		if (!ordersInCart || ordersInCart.length === 0) {
			return res.status(404).send("No products in cart");
		}

		let ordersWithSubtotal = [];

		for(const order of ordersInCart) {
			const productDetail = await Product.findById(order.productId);
			const subtotal = order.quantity * productDetail.price;
			ordersWithSubtotal.push({ ...order.toObject(), subtotal });
		}

		return res.send(ordersWithSubtotal);
	} catch (error) {
		console.error("Error getting the subtotal for each item:", error);
		return res.status(500).send("Error getting the subtotal for each item");
	}
};

// Total price for all items
module.exports.getTotalPrice = async (req, res) => {
	const { id: userId } = auth.decode(req.headers.authorization);
	try {
		const existingOrder = await Order.findOne({userId});
		if ( !existingOrder || existingOrder.products.length === 0) {
			return res.status(404).send({message: "There is no existing order"});
		} 

		let totalPrice = `The total price of your orders is ${existingOrder.totalAmount.toString()} pesos`;
		return res.send({totalAmount: existingOrder.totalAmount});

	} catch (error) {
		console.error("Error getting the total amount of your order:", error);
		return res.status(500).send("Error getting the total amount of your order");
	}
};

// Remove all items on cart (authenticated users only)
module.exports.removeCart = async( req, res ) => {
	const { id: userId } = auth.decode(req.headers.authorization);

	try{
		const cart = await Order.findOne({userId});
		if (!cart || cart.products.length === 0) {
			return res.status(404).send("There is no products on cart")
		}

		await Order.deleteOne({userId});
		
		res.send(cart);
	} catch (error) {
		console.error("Error removing products on cart:", error);
		return res.status(500).send("Error removing products on cart");
	}	
}