const Order = require("../models/OrderModel");
const PurchasedOrder = require("../models/PurchasedOrderModel");

const auth = require("../auth");

// checkout function
module.exports.checkout = async (req, res) => {
	const { id: userId } = auth.decode(req.headers.authorization);

	try {
		// check if there is existing products on cart
		const cart = await Order.findOne({userId});
		if (!cart || cart.products.length === 0) {
			return res.status(404).send("There is no products on cart")
		}

		const products = cart.products;
		const totalAmount = cart.totalAmount;

		// Create new purchase
		const newPurchase = new PurchasedOrder({
			userId,
			products,
			totalAmount
		})
		await newPurchase.save();
		return res.send(newPurchase);
	} catch (error) {
		console.error("Error adding order:", error);
		return res.status(500).send("Error adding order");
	}
}

// Retrieve all purchases (authenticated user only)
module.exports.getAllPurchases = async (req, res) => {
	const { id: userId } = auth.decode(req.headers.authorization);

	try{
		const purchasedOrder = await PurchasedOrder.find({userId});

		if (!purchasedOrder) {
			return res.status(404).send("There is no existing Purchased Order");
		}

		return res.send(purchasedOrder);

	} catch (error) {
		console.error("Error getting all purchased orders:", error);
		return res.status(500).send("Error getting all purchased orders");
	}
};

module.exports.getSinglePurchase = async(req, res) => {
	const { id, isAdmin } = auth.decode(req.headers.authorization);
	const purchaseOrderId = req.params.purchaseOrderId
	const userId = req.params.userId

	console.log(purchaseOrderId);
	try {
		if ( !isAdmin && userId !== id ) {
			return res.status(403).send("You are not authorized to perform this action");
		}

		const singlePurchasedOrder = await PurchasedOrder.findById(purchaseOrderId);

		if (!singlePurchasedOrder) {
			return res.status(404).send("There is no existing Purchased Order");
		}
		console.log("hi");
		return res.send(singlePurchasedOrder);
	} catch (error) {
		console.error("Error getting single purchase:", error);
		return res.status(500).send("Error getting single purchase");
	}
}

// Retrieve all purchases (admin user only)
module.exports.getAllPurchasedOrders = async (req, res) => {
	const { isAdmin } = auth.decode(req.headers.authorization);

	try{
    	if (!isAdmin) {
			return res.status(403).send('You are not an admin');
    	}
    	
		const purchasedOrder = await PurchasedOrder.find();

		if (!purchasedOrder) {
			return res.status(404).send("There is no existing Purchased Order");
		}

		return res.send(purchasedOrder);

	} catch (error) {
		console.error("Error getting all purchased orders:", error);
		return res.status(500).send("Error getting all purchased orders");
	}
};