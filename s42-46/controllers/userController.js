const User = require("../models/UserModel");
const Order = require("../models/OrderModel");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const validator = require("validator");

module.exports.registerUser = async (req, res) => {
	const {email, password} = req.body;

	if (!email || !password) {
		return res.status(400).send('All fields must be filled');
	}

	if (!validator.isEmail(email)) {
		return res.status(400).send('Email is not valid');
	}

	if (!validator.isStrongPassword(password)) {
		return res.status(400).send('Strong Password Required: Minimum 8 characters, 1 number, 1 capital letter, 1 special character.');
	}

	try {
		const exist = await User.findOne({email});
		if (exist) {
			return res.status(400).send('Email already in use');
		}

		const newUser = new User ({
			...req.body,
			password: bcrypt.hashSync(password, 10)
		})

		const savedUser = await newUser.save();
		res.send(savedUser);
	} catch (error) {
		res.status(500).send(error.message);
	}

}


module.exports.loginUser = (req, res) => {
	const {email, password} = req.body;

	if (!email || !password) {
		return res.status(400).send('All fields must be filled');
	}

	return User.findOne({email}).then(result => {

		// User does not exist
		if(result == null){
			console.log("User does not exist");
			res.status(404).send("User does not exist"); 

		// User exists
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			// If the password matches
			if(isPasswordCorrect){
				res.send({ access: auth.createAccessToken(result)}); 
			} else {
				console.log("Password did not match");
				res.status(401).send("Password did not match");
			}
		}
	})
};

// Retrieve User Details
module.exports.getUserDetails = async (req, res) => {
	try {
		const { id: userId, isAdmin } = auth.decode(req.headers.authorization);
		/*const userId = req.params.userId;*/

		const user = await User.findById(userId);
		if (user) {
			user.password = "";

			const orderedProduct = await Order.find({ userId });
			if (orderedProduct.length > 0) {
				const userWithOrders = { ...user.toObject(), orderedProduct };
				res.send(userWithOrders);
			} else {
				res.send({...user.toObject(), orderedProduct: `User ${user.email} has no order` });
			}

		} else {
			res.status(404).send("User not found");
		}

	} catch (error) {
		res.status(500).send(error.message);
	}
};

// set user as admin (admin only)
module.exports.setAdminStatus = async (req, res) => {
	try{
		const { isAdmin } = auth.decode(req.headers.authorization);

		if (!isAdmin) {
			return res.status(403).send("You are not an admin");
		}

		let userId = req.params.userId;

		let user = await User.findById(userId);
		if (!user) {
			return res.status(404).send("User not found");
		}

		if (user.isAdmin) {
			return res.send("User is already an admin");
		}

		user.isAdmin = true;
		const updateUser = await user.save();

		res.send(updateUser);

	} catch (error) {
		console.error('Error setting user as admin', error);
		res.status(500).send('Error setting user as admin');
	}
};

// get user orders
module.exports.getUserOrders = async (req, res) => {
	try {
		const { id: userId } = auth.decode(req.headers.authorization);

		const user = await User.findById(userId);
		if (user) {

			const orders = await Order.find({ userId });
			if (orders.length > 0) {
				res.send(orders);
			} else {
				res.send(`User ${user.email} has no order`);
			}

		} else {
			res.status(404).send("User not found");
		}

	} catch (error) {
		res.status(500).send(error.message);
	}
};

// Retrieve Specific User Details (admin only)
module.exports.getSingleUserDetails = async (req, res) => {
	try {
		const { isAdmin } = auth.decode(req.headers.authorization);
		const { userId } = req.params;

		const user = await User.findById(userId);
		if (user) {
			user.password = "";

			const orderedProduct = await Order.find({ userId });
			if (orderedProduct.length > 0) {
				const userWithOrders = { ...user.toObject(), orderedProduct };
				res.send(userWithOrders);
			} else {
				res.send({...user.toObject(), orderedProduct: `User ${user.email} has no order` });
			}

		} else {
			res.status(404).send("User not found");
		}

	} catch (error) {
		res.status(500).send(error.message);
	}
};

// Retrieve All User Details (admin only)
module.exports.getAllUserDetails = async (req, res) => {
	try {
		const { id: userId, isAdmin } = auth.decode(req.headers.authorization);

		if (!isAdmin) {
			return res.status(403).send("You are not an admin");
		}

			const users = await User.find();
			users.map(user => {
				user.password = "";
			})
			res.send( users );

	} catch (error) {
		res.status(500).send(error.message);
	}
};

// Set admin as user (admin only)
module.exports.setAdminAsUser = async (req, res) => {
	try{
		const { isAdmin } = auth.decode(req.headers.authorization);

		if ( !isAdmin ) {
			return res.status(403).send("You are not an admin");
		}

		let userId = req.params.userId;

		let user = await User.findById(userId);
		if ( !user ) {
			return res.status(404).send("User not found");
		}

		if ( !user.isAdmin ) {
			return res.send("User is not an admin already");
		}

		user.isAdmin = false;
		const updateUser = await user.save();

		res.send(updateUser);

	} catch (error) {
		console.error('Error setting admin as user', error);
		res.status(500).send('Error setting admin as user');
	}
};