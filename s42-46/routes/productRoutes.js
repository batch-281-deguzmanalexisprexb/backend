const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")
const auth = require("../auth")

router.post("/", auth.verify, productController.addProduct);

router.get("/all", auth.verify, productController.getAllProducts);

router.get("/allActive", productController.getAllActive);

router.get("/:productId", productController.getSingleProduct);

router.put("/:productId", auth.verify, productController.updateProduct);

router.patch("/archive/:productId", auth.verify, productController.archiveProduct);

router.patch("/activate/:productId", auth.verify, productController.activateProduct);

router.delete("/delete/:productId", auth.verify, productController.deleteProduct);

module.exports = router;