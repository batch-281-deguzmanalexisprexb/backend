const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require("../auth");

router.post("/checkout", auth.verify, orderController.createOrder);

router.get("/all", auth.verify, orderController.getAllOrders);

router.patch("/addToCart", auth.verify, orderController.addToCart);

router.patch("/changeQuantity", auth.verify, orderController.changeProductQuantity);

router.patch("/:productId/removeProducts", auth.verify, orderController.removeProduct);

router.get("/getSubtotal", auth.verify, orderController.getSubtotal);

router.get("/totalPrice", auth.verify, orderController.getTotalPrice);

router.delete("/deleteCart", auth.verify, orderController.removeCart);

module.exports = router;