const express = require("express");
const router = express.Router();
const purchasedOrderController = require("../controllers/purchasedOrderController")
const auth = require("../auth");

router.post("/checkout", auth.verify, purchasedOrderController.checkout);

router.get("/allPurchases", auth.verify, purchasedOrderController.getAllPurchases);

router.get("/singlePurchase/:userId/:purchaseOrderId", auth.verify, purchasedOrderController.getSinglePurchase);

router.get("/allPurchasedOrders", auth.verify, purchasedOrderController.getAllPurchasedOrders);

module.exports = router;