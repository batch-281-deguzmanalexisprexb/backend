const express = require("express");
const router = express.Router();

const auth = require("../auth");

const userController = require("../controllers/userController");

router.post("/register", userController.registerUser);

router.post("/login", userController.loginUser);

router.get("/userDetails", auth.verify, userController.getUserDetails);

router.patch("/:userId/setAsAdmin", auth.verify, userController.setAdminStatus);

router.patch("/:userId/setAsUser", auth.verify, userController.setAdminAsUser);

router.get("/myOrders", auth.verify, userController.getUserOrders);

router.get("/:userId/singleUserDetails", auth.verify, userController.getSingleUserDetails);

router.get("/allUserDetails", auth.verify, userController.getAllUserDetails);

module.exports = router;