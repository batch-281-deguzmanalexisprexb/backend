console.log("Hello");

/*General Instructions:
1. Upon using the register function, we can add a new user into the registered users array. However, if there is a duplicate, we will alert the user.

2. Upon using the add friend function, we can add registered user in our friendsList array. However, if the user is not found in the registeredUsers array, we will alert the user.


3. Upon using the display friends function, we can display each user from our friendsList array. However, if there is no user in the friendsList, we will alert the user.


4. Upon using the display number of friends function, we can show the total number of friends in our friends list. However, if there is no user in the list, we will alert the user.


5. Upon using the delete friend function, we can delete the last item in our friends list. However, if there is no more user in the list, we will alert the user.



Specific Instructions:
1. In the S22 folder, create an a1 folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Copy the activity code and instructions from your Boodle Notes into your index.js.
4. Create a function which will allow us to register into the registeredUsers list.
- This function should be able to receive a string.
- Determine if the input username already exists in our registeredUsers array.
~ If it is, show an alert window with the following message:
"Registration failed. Username already exists!"
~ If it is not, add the new username into the registeredUsers array and show an alert:
"Thank you for registering!"
- Invoke and register a new user in the browser console.
- In the browser console, log the registeredUsers array.
4. Create a function which will allow us to add a registered user into our friends list.
- This function should be able to receive a string.
- Determine if the input username exists in our registeredUsers array.
~ If it is, add the foundUser in our friendList array. -Then show an alert with the following message: - 
"You have added <registeredUser> as a friend!"
~ If it is not, show an alert window with the following message:
"User not found."
- Invoke the function and add a registered user in your friendsList in the browser console.
- In the browser console, log the friendsList array in the console.
5. Create a function which will allow us to show/display the items in the friendList one by one on our console.
- If the friendsList is empty show an alert:
"You currently have 0 friends. Add one first."
- Invoke the function in the browser console.
6. Create a function which will display the amount of registered users in your friendsList.
- If the friendsList is empty show an alert:
"You currently have 0 friends. Add one first."
- If the friendsList is not empty show an alert:
"You currently have friends."
- Invoke the function in the browser console.
7. Create a function which will delete the last registeredUser you have added in the friendsList.
- If the friendsList is empty show an alert:
"You currently have 0 friends. Add one first."
- Invoke the function in the browser console.
- In the browser console, log the friendsList array.
8. Create a git repository named S22.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.*/

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

const register = user => {
	if(registeredUsers.includes(user)) {
		alert("Registration failed. Username already exists!");
	} else {
		registeredUsers.push(user);
		alert("Thank you for registering!");
		// return registeredUsers;
	}
};


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/
const addFriend = friend => {
	if(registeredUsers.includes(friend)) {
		alert(`You have added ${friend} as a friend!`);
		friendsList.push(friend);
	} else {
		alert(`User not found.`);
	}
};

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
const displayFriends = () => {
	if(friendsList.length === 0) {
		alert("You currently have 0 friends. Add one first.");
	} else {
		friendsList.forEach(friend => console.log(friend));
	}
};


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
const displayNumberOfFriends = () => {
	if(friendsList.length === 0) {
		alert("You currently have 0 friends. Add one first.");
	} else if (friendsList.length !== 0) {
		alert(`You currently have ${friendsList.length} friends.`);
	}
};

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
/*const deleteFriend = function() {
	if (friendsList.length === 0) {
		alert("You currently have 0 friends. Add one first.");
	} else {
		friendsList.pop();
	}
};*/

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

const deleteFriend = friend => {
	if (friendsList.length === 0) {
		alert("You currently have 0 friends. Add one first.");
	} else if (!friend) {
		friendsList.pop();
	} else if (!friendsList.includes(friend)) {
		alert(`You do not have a friend named ${friend}.`);
	} else {
		friendsList.splice(friendsList.indexOf(friend), 1);
	}
};



