// console.log("Hello");

/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

//Array
/*"cities" : [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"}, 
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}, 
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"}
]*/

/*"dogs" : [
	{"name": "Loki", "age": "5", "breed": "Golden Retreiver"}, 
	{"name": "Skye", "age": "1", "breed": "GolShih-tzuer"}, 
	{"name": "Teddy", "age": "5", "breed": "Shih-tzu"}
]*/

let batchesArr = [{ batchName: 'Batch X'}, {batchName: 'Batch Y'}];

console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data);

/*let firstName = prompt('What is your first name?')
let lastName = prompt('What is your last name?')
let age = prompt('What is your age?')
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);*/

/*let brand = prompt('Please enter car brand:');
let type = prompt('Please enter car type: ');
let year = prompt('Please enter manufacture year:');

let carData = JSON.stringify({
	brand: brand,
	type: type,
	year: year,
})

console.log(carData);
*/


let batchesJSON = `[{ "batchName": "Batch X"}, { "batchName": "Batch Y"}]`

console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));