const http = require('http');

// Create a variable "port" to store the port number

const port = 4000;

const app = http.createServer((req, res) => {
	// Accessing the "greeting" route returns a message of "Hello, World!"
	if(req.url == '/greeting') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("Hello Again");
	} else if (req.url == '/homepage'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end("This is the homepage");
	} else {
		res.writeHead(404, {'Content-Type': 'text/plain'});
		res.end('Page not availble');
	}
});

app.listen(port);

console.log(`Server now accessible at localhost: ${port}`);