// Using require directive to load http module
const http = require("http");

// Create a server

http.createServer(function(reqest, response) {
	// returning what type fo response being thrown to the client
	response.writeHead(200, {'Content-Type': 'text/plain'}); //response type that will be returned
	// Send the response with the text content 'Hello, World!'
	response.end('Hello, Wordl!');
}).listen(4000);

// Whens erver is running, console will print the message:
console.log("Server is running at localhost: 4000");























/*
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/plain");
  res.end("Hello, World!");
});

server.listen(3000, "localhost", () => {
  console.log("Server listening on port 3000");
});



const server = http.createServer((req, res) => {
  res.writeHead(200, {
    'Content-Type': 'text/plain',
    'Custom-Header': 'Some value'
  });
  res.end('Hello, World!');
});

server.listen(3000, () => {
  console.log('Server listening on port 3000');
});
*/

