const http = require('http');

// Mock database
let mockDatabase = [
	{
		"productName": "Mug",
		"stock": "50",
		"isAvailable": true
	},
	{
		"productName": "Mug",
		"stock": "50",
		"isAvailable": true
	}	
];

let port = 4000;

let app =  http.createServer((request, response) => {
	if(request.url == "/items" && request.method == "GET") {
		response.writeHead(200, {"Content-Type": "application/json"} );
		response.write(JSON.stringify(mockDatabase));
		response.end();
	}

	// Route
	if(request.url == "/items" && request.method == "POST") {
		let requestBody = "";

		// stream
		request.on("data", (chunk) => {
			requestBody += chunk;
		})

		request.on("end", () => {
			console.log(requestBody);;
			requestBody = JSON.parse(requestBody);

			let newItems = {
				"productName": requestBody.productName,
				"stock": requestBody.stock,
				"isAvailable": requestBody.isAvailable
			};

			mockDatabase.push(newItems);
			console.log(mockDatabase);

			response.writeHead(200, {"Content-Type": "application/json"});
			response.write(JSON.stringify(requestBody));
			response.end();

		})
	}
});

app.listen(port, () => console.log("Server is running at localhost: 4000"));