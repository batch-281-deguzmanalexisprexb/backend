console.log('hello')

let grades = [98.5, 94.3, 89.2, 90.1];

let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

let tasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];

let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Mumbai';

let cities = [city1, city2, city3];

console.log(tasks);
console.log(cities);

console.log('Array length');
console.log(tasks);
console.log(cities);

let fullName = "Aizaac Estiva"
console.log(fullName.length);

tasks.length = tasks.length - 1;
console.log(tasks.length);
console.log(tasks);

cities.length--;
console.log(cities);

fullName.length - fullName.length -1;
console.log(fullName.length);

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles.length++;
console.log(theBeatles);

console.log(grades[0]);
console.log(computerBrands[3]);
console.log(grades[20]);

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker = lakersLegends[2];
console.log('Accessing arrays using variables');
console.log(currentLaker);

console.log('Array before reassignment:');
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log('Array reassignment:');
console.log(lakersLegends);

let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];

let lastElementIndex = bullsLegends.length - 1;

console.log(bullsLegends[lastElementIndex]);

console.log(bullsLegends[bullsLegends.length - 1]);

let newArr = [];
console.log(newArr[0]);

newArr[0] = 'Cloud Strife';
console.log(newArr);

/*let newVar = prompt('Enter a name: ');
newArr[1] = newVar;
console.log(newArr);*/

for(let index = 0; index < computerBrands.length; index++){
	console.log(computerBrands[index]);
}

let numArr = [5, 12, 30, 46, 40];

for(let i = 0 ; i < numArr.length; i++){
	if(numArr[i] % 5 === 0) {
		console.log(numArr[i] + ' is divisible by 5');
	} else {
		console.log(numArr[i] + 'is not divisble by 5');
	}
}



let twoDim = [['Kenzo', 'Alonzo'], ['Bella', 'Aizaac']];

console.log(twoDim[0][1]);
console.log(twoDim[1][0]);
console.log(twoDim[1][1]);

console.log(twodim.flat());