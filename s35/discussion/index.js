const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://deguzmanalexisprexb:nbOhpp4C8Yj7Ritu@wdc028-course-booking.ro6ls2b.mongodb.net/b281_to-do?retryWrites=true&w=majority",
	{ //depricators
		useNewUrlParser : true,  
		useUnifiedTopology : true
	}
)

// Set notification for connection success or failure
let db = mongoose.connection;

// If a connection error occurred, output in the console 
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Mongoose Schemas
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

const userSchema = new mongoose.Schema({
	username : String,
	password : String
})

// Models [Task models]
const Task = mongoose.model("Task", taskSchema)
const User = mongoose.model("User", userSchema)

// Allow the app to read json data
app.use(express.json());
// Allows the app to read data from forms
app.use(express.urlencoded({extended: true}));

// Creation of todo list routes

// Creating a new task
app.post("/tasks", (req, res) => {
	Task.findOne({name : req.body.name}).then((result, err) => {
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task ({
				name : req.body.name
			});

			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

// Get all the tasks 
app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		if(err){
			// Will print any errors found in the console 
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})

app.post("/signup", (req, res) => {
	User.findOne({username : req.body.username}).then((result, err) => {
		if(result != null && result.username == req.body.username){
			return res.send("Duplicate username found");
		} else {
			let newUser = new User ({
				username : req.body.username,
				password : req.body.password
			});

			newUser.save().then((savedUser, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New User created");
				}
			})
		}
	})
})

// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));