const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// 1. Create a GET route that will access the /home route that will send a
// response with a simple message:
// 	a. "Welcome to the home page"
app.get("/home", (req, res) => {
	res.send("Welcome to the home page")
})
// 2. Process a GET request at the /home route using postman.


// 3. Create a GET route that will access the /users route that will send a the users array as a response.
let users = [
	{
		"username": "johndoe",
		"password": "johndoe1234",
	}	
];
app.get("/users", (req, res) => {
	res.send(users)
})
// 4.Process a GET request at the /users route using postman.


// 5. Create a DELETE route that will access the /delete-user route to
// remove a user from the mock database.



// 6. The delete-user route logic is as follows:
// a. Create a variable to store the message to be sent back to the client/Postman
// b. Create a condition to check if there are users found in the array
// 	i. If there is, create a for loop that will loop through the elements of the "users" array
// 		1. Add an if statement that if the username provided in the request body and the username of the current object in the loop is the same
// 			a. If it is, remove the current object from the array. You can use splice array method.
// 			b. Change the message to be sent back by the response:
// 				i. "User <username> has been deleted."
// 			C. Break the loop.
/*
6. The delete-user route logic is as follows:
		1. Outside the loop, add an if statement that if the message variable is undefined, update the message variable with the following message:
		a."User does not exist."
	C. Add an else statement if there are no users in the array, update the message variable
	with the following message:
		a."No users found."
	d. Send a the message variable as a response back to the client/Postman once the user has been deleted or if a user is not found
*/

app.delete("/delete-user", (req, res) => {
	let message;

	if (users.length > 0) {
		for(let i = 0; i<users.length; i++){
			if (req.body.username == users[i].username) {
				let userToRemove = req.body.username;
				let indexOfUser = users.indexOf(userToRemove)
				users.splice(indexOfUser, 1);
				message = `User ${req.body.username} has been deleted`;
				res.send(message);
				break;
			} 
		}
		if(!message) {
		message = "User does not exist."
		}
	} else {
		message = "No users found."
	} 

	res.send(message);
	console.log(users)
	
})







if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}

module.exports = app;