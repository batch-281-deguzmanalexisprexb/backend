console.log('Hi, World!');

let x = 1397;
let y = 7831;

let sum = x + y;
console.log(`Result of a ddition operator: ${sum}`);

let difference = x - y;
console.log(`Result of subtraction operator: ${difference}`);

let product = x * y;
console.log(`Result of multiplication operator: ${product}`);

let quotient = x / y;
console.log(`Result of division operator: ${quotient}`);

let remainder = y % x;
console.log(`Result of modulo operator: ${remainder}`);

let assignmentNumber = 8;

assignmentNumber += 2;
console.log(`Result of addition assignment Operator: ${assignmentNumber}`); //==> 10
assignmentNumber -= 2;
console.log(`Result of subtraction assignment Operator: ${assignmentNumber}`); //==> 8
assignmentNumber *= 2;
console.log(`Result of multiplication assignment Operator: ${assignmentNumber}`); //==> 16
assignmentNumber /= 2;
console.log(`Result of division assignment Operator: ${assignmentNumber}`); //==> 8

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(`Result of mdas operation: ${mdas}`); //==> 0.6000000000000001

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(`Result of pemdas operation: ${pemdas}`); //==> 0.19999999999999996

pemdas = (1 + (2 -3)) * (4 / 5);
console.log(`Result of pemdas operation: ${pemdas}`); //==> 0


				/******************************INCREMENT DECREMENT*****************************/
let z = 2;
let increment = ++z;
console.log(`Result of pre-increment: ${increment}`); //==> 3
console.log(`Result of pre-increment: ${z}`); //==> 3

increment = z++;
console.log(`Result of pre-increment: ${increment}`); //==> 3
console.log(`Result of pre-increment: ${z}`); //==> 4

let decrement = --z;
console.log(`Result of pre-decrement: ${decrement}`); //==> 3
console.log(`Result of pre-decrement: ${z}`); //==> 3

decrement = z--;
console.log(`Result of pre-decrement: ${decrement}`); //==> 3
console.log(`Result of pre-decrement: ${z}`); //==> 2

				/******************************TYPE COERCION*****************************/
let numA = '10';
let numB = 12

let coercion = numA + numB;
console.log(coercion); //==> 1012
console.log(typeof coercion); //==> String

let numC = true + 1;
console.log(numC); //==> 2

				/******************************COMPARISSON OPERATORS*****************************/
let juan = 'juan';

console.log(1 == 1); //==> true
console.log(1 == 2); //==> false
console.log(1 == '1'); //==> true
console.log(0 == ''); //==> true
console.log('juan' == juan); //==> true

console.log(1 != 1); //==> false
console.log(1 != 2); //==> true
console.log(1 != '1'); //==> false
console.log(0 != ''); //==> false
console.log('juan' != juan); //==> false

console.log(1 === 1); //==> true
console.log(1 === 2); //==> false
console.log(1 === '1'); //==> false
console.log(0 === ''); //==> false
console.log('juan' === juan); //==> true

console.log(1 !== 1); //==> false
console.log(1 !== 2); //==> true
console.log(1 !== '1'); //==> true
console.log(0 !== ''); //==> true
console.log('juan' !== juan); //==> false

//Relational Operator
let a = 50;
let b = 65;

let isGreaterThan = a > b;
let isLessThan = a < b;
let isGTorEqual = a >= b;
let isLTorEqual = a <= b;

console.log(isGreaterThan); //==> false
console.log(isLessThan); //==> true
console.log(isGTorEqual); //==> false
console.log(isLTorEqual); //==> true

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log(`Result of logical AND Operator: ${allRequirementsMet}`);

let someRequirementsMet = isLegalAge || isRegistered;
console.log(`Result of logical OR Operator: ${someRequirementsMet}`);

let someRequirementsNotMet = !isRegistered;
console.log(`Result of logical NOT Operator: ${someRequirementsNotMet}`);