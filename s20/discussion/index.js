console.log("hello");

// While loop
/*
	- it evaluates a condition, if returned truem it will execute statements as long as the condition is satisfied. If at first the condition is not satisfied, no statement will be executed.
	Syntax: 
		while (conidition) {
			statement;
		}
*/

let count = 5;

while (count !== 0) {
	console.log(`While: ${count}`);
	count--;
}

while (count !== 5) {
	count++;
	console.log(`While: ${count}`);
}

// do-while loop
/*
	-iterates statements within a number of times based on a condition. However, if the condition was not satisfied at first, one statement will be executed.
	Syntax:
		do{
			statement
		} while(condition);
*/

/*let number = +(prompt("Give me a number"));

do{
	console.log(`Do while: ${number}`);

	number += 1;
} while (number < 10);*/

let even = 2;

do {
	console.log(even);
	even += 2;
}while (even <= 10);


//For Loop
/*
	- a looping construct that is more flexible than other loops. It consists of three parts:
		1 initialization
		2 expression/condition
		3 final expression/step expression

	Syntax:
		for (initialization; condition; step expression) {
			statements
		}
*/

for (let count = 0; count <= 20; count++) {
	console.log(count);
}

let myString = "alex";

console.log(myString.length);

console.log(myString);

for(let i = 0; i < myString.length; i++){
	console.log(myString[i]);
}

let myName = "Alexis";
console.log("Looping trough consonants");

for(let i = 0; i < myName.length; i++) {
	if(
		myName[i].toLowerCase() == 'a' || 
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u' 
	) {
		console.log(3)
	} else {
		console.log(myName[i]);
	}
}

for(let count = 0 ; count <= 20; count++) {
	if(count % 2 === 0){

		continue;
	}

	console.log(`Continue and Break: ${count}`);

	if(count > 10) {
		break;
	}
}

let name = "alexandro";

for(let i = 0 ; i < name.length ; i++) {
	console.log(name[i]);

	if(name[i].toLowerCase() === 'a') {
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i].toLowerCase() === 'd') {
		break
	}
}