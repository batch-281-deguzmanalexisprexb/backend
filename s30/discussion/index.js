// Aggregation

//Create documents for fruits database
db.fruits.insertMany([
	{
		"name": "Apple",
		"color": "Red",
		"stock": 20,
		"price": 40,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["Philippines", "US"]
	},
	{
		"name": "Bannana",
		"color": "Yellow",
		"stock": 15,
		"price": 20,
		"supplier_id": 2,
		"onSale": true,
		"origin": ["Philippines", "Ecuador"]
	},
	{
		"name": "Kiwi",
		"color": "Green",
		"stock": 25,
		"price": 50,
		"supplier_id": 1,
		"onSale": true,
		"origin": ["US", "China"]
	},
	{
		"name": "Mango",
		"color": "Yellow",
		"stock": 10,
		"price": 120,
		"supplier_id": 2,
		"onSale": false,
		"origin": ["Philippines", "India"]
	}
]);

// MongoDb Aggregation

/*
	- Used to generate manipulat data and perform operations to create filtered results

	Using the aggregate methods

	- "$match" - used to pass documents that meet specified conditions
	- Syntax
		- { $match : { field: value} }

	- "$group" - used to group elements together and field-value pairs using the data from the grouped elements
	- Syntax
		db.collectionName.aggregate([
			{ $match : { field: value } }
			{ $group: {_id: $fieldB}, { result : { operation }}}
		])


*/
/*	
	db.collection.aggregate([
	  { $match: { <query> } },  // Optional: Filter documents based on a condition
	  { $group: {
	    _id: <expression or field>,  // The grouping key or expression
	    field1: { <accumulator1> : <expression1> },  // Aggregate field1 using accumulator1
	    field2: { <accumulator2> : <expression2> },  // Aggregate field2 using accumulator2
	    ...
	  }}
	])
*/


db.fruits.aggregate([
	{ $match: {"onSale": true }},
	{ $group: { 
		_id: "$supplier_id",
		total: { $sum: "$stock" }
		}
	}
]);

//Field Projections with aggregation

/*
	$project - used when aggregating data to include/exclude fields from the returned results
	Syntax: 
		- { $project : { field: 1/0} }
			1 - show
			0 - not show


*/

db.fruits.aggregate([
	{ $match: {"onSale": true }},
	{ $group: { 
		_id: "$supplier_id",
		total: { $sum: "$stock" }
		}
	},
	{ $project: { _id: 0}}
]);



// Sorting aggregated results

/*
	$sort - used to change the order of aggregated results
	Syntax:
		{ $sort: { field: 1/-1 } }
		
			1 - ascending order
			2 - descending order
*/

db.fruits.aggregate([
	{ $match: {"onSale": true }},
	{ $group: { 
		_id: "$supplier_id",
		total: { $sum: "$stock" }
		}
	},
	{ $sort: { total: -1}}
]);


// aggregating results based on array fields

/*
	$unwind - DECONSTRUCTS an array field from a collection/field with an array value to output a result for each element.
	Syntax:
		{ $unwind: field }
*/

db.fruits.aggregate([
	{ $unwind: $origin }
]);

// Displays fruit dcuments by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
	{ $unwind: "$origin" },
	{ $group: { 
		_id: "$origin", 
		kinds: { $sum: 1 } 
	} }
]);



// One-to-one Relationship
// Create an id and stores it in the variable owner
var owner = ObjectId();

// Create and "owner" document that uses the generate id
db.owners.insert({
	_id: owner,
	name: "John Smith",
	contact: "09871236545"
});

// Change the "<owner_id>" using the actual id in the previously created document

db.suppliers.insertOne({
	name: "ABC fruits",
	contact: "09193219878",
	owner_id: ObjectId("646b5a467c00f70d8caabc73")
});


// One-to-few relationship
db.suppliers.insertOne({
	name: "DEF Fruits",
	contact: "09179873212",
	addresses: [
		{ street: "123 San Jose St", city: "Manila" },
		{ street: "367 Gill Puyat", city: "Makati" }
	]
});

// One-to-many Relationship
var supplier = ObjectId();
var branch1 = ObjectId();
var branch2 = ObjectId();

db.suppliers.insertOne({
	_id: supplier,
	name: "GHI Fruits",
	contact: "09191234567",
	branches: [
		branch1
	]
})


// branches collection
db.branches.insertOne({
	_id: branch1,
	name: "BF Homes",
	address: "123 Arcardio Santos St.",
	city: "Paranaque",
	supplier_id: ObjectId("646b61bc98d8690e062f9538") //id generated from the last insertOne() code
});

db.branches.insertOne({
	_id: branch2,
	name: "Rizal",
	address: "123 San Jose St.",
	city: "Manila",
	supplier_id: ObjectId("646b61bc98d8690e062f9538") 
});





































db.products.aggregate([
  {
    $project: {
      productName: 1,
      totalPrice: { $multiply: ["$price", "$quantity"] }
    }
  }
])


db.collection.aggregate([
  {
    $project: {
      field1: { $add: ["$field2", "$field3"] }, // Addition operator
      field2: { $subtract: ["$field4", "$field5"] }, // Subtraction operator
      field3: { $multiply: ["$field6", "$field7"] }, // Multiplication operator
      field4: { $divide: ["$field8", "$field9"] }, // Division operator
    }
  }
])
